(function () {
  'use strict';

  angular
    .module('beat')
    .directive('beatboxActionbar', beatboxActionbar)
    .directive('beatboxActionCommand', beatboxActionCommand);

  /** @ngInject */
  function beatboxActionCommand() {
    var directive = {
      restrict: 'A',
      scope: {
        callback: '&beatboxActionCallback'
      }
      ,
      link: actionCommandLinkFn
    };

    return directive;


    function actionCommandLinkFn(scope, element, attrs, controllers) {
      var cmd = attrs.beatboxActionCommand;

      if (!cmd) {
        throw new Error('beatbox-action-command="???" - Command required!');
      }

      var callback = scope.callback();

      /* if(!callback){
       throw new Error('beatbox-action-callback="???" - Callback required!');
       }
       */
      var events = angular.isArray(attrs.events) ? attrs.event : (attrs.events || ['click']);


      angular.forEach(events, function (evt) {
        element.on(evt, function () {
          _emitEvent(evt);
        })
      });


      function _emitEvent(eventType) {
        scope.$emit('beat:action:command', {
          command: cmd,
          src: element,
          event: eventType,
          callback: scope.callback()
        });
      }
    }
  }

  /** @ngInject */
  function beatboxActionbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/beatbox-actionbar/beatbox-actionbar-tpl.html',
      scope: {},
      controller: BeatboxActionBarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function BeatboxActionBarController($scope, $log, $aside) {
      var vm = this;
      vm.model = {};

      // "vm.creationDate" is available by directive option "bindToController: true"
      $scope.$on('beatbox:appOnStage', function (event, data) {
        vm.model.onStage = data;
        //debugger
      });


      $scope.$on('beatbox:action:command', function (event, command) {
        $log.debug("Hurra :" + angular.toJson(command, true));
        vm.model.onCommand = true;
        var asideInstance = $aside.open({
          templateUrl: 'app/components/beatbox-actionbar/command/' + command.command + '.html',
          controller: modalController,
          //scope:false,
          bingToController: true,
          controllerAs: 'vm',
          placement: 'bottom',
          size: 'lg'
        });

        /** @ngInject */
        function modalController ($scope) {
          var vm = this;
          vm.output = undefined;

          vm.done = function () {
            $scope.$close(vm.output);
          }
        }
        asideInstance.result.then(function (output) {
          $log.debug('Modal closed out[' + angular.toJson(output, true) + ']');
          if (command.callback) {
            //debugger
            command.callback(output);
          }

        });
        asideInstance.closed.then(function () {
          vm.model.onCommand = false;
        })
      });
    }
  }

})();
