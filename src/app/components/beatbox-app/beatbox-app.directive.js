(function () {
  'use strict';

  angular
    .module('beat')
    .directive('beatboxApp', beatboxApp);

  /** @ngInject */
  function beatboxApp() {
    var directive = {
      restrict: 'EA',
      templateUrl: 'app/components/beatbox-app/beatbox-app-tpl.html',
      scope: {},
      require:['beatboxApp','^beatboxStage'],
      link: beatboxAppLink,
      controller: BeatboxAppController,
      controllerAs: 'vm',
      //replace:true,
      bindToController : {
        model: '=appModel'
      }
      //bindToController: true
    };

    return directive;

    /** @ngInject */
    function beatboxAppLink(scope, element, attrs, controllers) {
      var parentCtrl =  controllers[1];
      var appCtrl = controllers[0];
      appCtrl.parent = parentCtrl;

      appCtrl.stageInfo = parentCtrl.api.registerAppBox({
        controller:appCtrl,
        element:element,
        scope:scope //TBD non dovrebbe servire
      })


    }

    /** @ngInject */
    function BeatboxAppController($scope, $element){
      var vm = this;

      vm.api = {
        onFocus: onFocus,
        onFocusLost : onFocusLost,
        requestFocus : requestFocus,
        close: closeMe,
        open:  openMe
      };

      vm.model.isFocused = false;


      function onFocus() {
        vm.model.isFocused = true;
      }

      function onFocusLost(){
        vm.model.isFocused = false;
      }

      function requestFocus(){
        //debugger
        $scope.$emit('beatbox:app:focusrequest',vm.stageInfo.index);
      }

      function closeMe(){

        vm.stageInfo.viewState = 'closed';
        $scope.$emit('beatbox:app:close',vm.stageInfo.index);
      }

      function openMe(){
        //debugger
        requestFocus();

        vm.stageInfo.viewState = 'opened';
        $scope.$emit('beatbox:app:open',vm.stageInfo.index);
      }

    }
  }

})();
