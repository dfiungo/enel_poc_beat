(function () {
  'use strict';

  angular
    .module('beat')
    .directive('beatboxMenu', beatboxMenu);

  /** @ngInject */
  function beatboxMenu() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/beatbox-menu/beatbox-menu-tpl.html',
      scope: {},
      controller: BeatboxMenuController,
      controllerAs: 'vm',
      bindToController: true,
    };

    return directive;

    /** @ngInject */
    function BeatboxMenuController($scope, $log) {
      var vm = this;

      vm.model = {
        onStage: undefined
      };

      $scope.$on('beatbox:appOnStage', function (event,data) {
        vm.model.onStage = data;
        //debugger
      });

    }
  }

})();
