(function () {
    'use strict';

    angular
        .module('beat')
        .directive('beatboxSidebar', beatboxSidebar);

    /** @ngInject */
    function beatboxSidebar($log) {
        return {
            templateUrl: 'app/components/beatbox-sidebar/beatbox-sidebar.html',
            restrict: 'E',
            scope: {},
            link: beatboxSidebarLinkFn,
            controllerAs: 'vm',
            controller: beatboxSidebarController
        }


        function beatboxSidebarController() {
            var vm = this;

            vm.model = {
                isOpened: false
            };

            vm.api = {
                open: open,
                close: close
            };

            function open() {
                vm.model.isOpened = true;
            }

            function close() {
                vm.model.isOpened = false;
            }

        }

        function beatboxSidebarLinkFn(scope, element, attrs, controller) {
            element.on('mouseenter', function () {
                $log.debug("Enter before:" + controller.model.isOpened);
                controller.api.open();
                scope.$apply();
                $log.debug(controller.model.isOpened);
            });

            element.on('mouseleave', function () {
                //debugger
                $log.debug("Leave before: "+ controller.model.isOpened);
                controller.api.close();
                scope.$apply();
                $log.debug(controller.model.isOpened);

            });


        }
    }

})();
