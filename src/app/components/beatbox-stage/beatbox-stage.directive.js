(function () {
  'use strict';

  angular
    .module('beat')
    .directive('beatboxStage', beatboxStage);

  /** @ngInject */
  function beatboxStage($log, $window) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/beatbox-stage/beatbox-stage-tpl.html',
      scope: {},
      controller: BeatboxStageController,
      link: beatboxStageLink,
      controllerAs: 'vm',
      bindToController: {
        apps: '='
      },
      replace: true
    };

    return directive;


    function beatboxStageLink(scope, element, attrs, controllers) {
      controllers.model.stageWidth = element.width();

      $window.addEventListener('resize', function () {
        if (element.width() != controllers.model.stageWidth) {
          controllers.model.stageWidth = element.width();
          scope.$broadcast('beatbox:stage:sizeChange', controllers.model.stageWidth);
        }
      });


    }

    /** @ngInject */
    function BeatboxStageController($scope, $log) {
      var vm = this;

      vm.config = {
        maxOnStage: 3,
        minimizedSize: 50,
        defaultMinSize: 200
      };

      vm.model = {
        beatApp: vm.apps,
        onStage: [],
        appFocus: undefined,
        stageWidth: -1
      };

      vm.api = {
        getAppConfig: getAppConfig,
        registerAppBox: registerAppBox

      };


      function getAppConfig(appId) {

      }

      function registerAppBox(appData) {
        var registrationData = {
          index: vm.model.onStage.length,
          viewState: 'opened',
          app: appData
        };
        vm.model.onStage.push(registrationData
        );

        if (vm.model.onStage.length === 1) {
          setFocus(0);
        } else {
          setFocus(-1);
        }
        return registrationData;
      }


      function setFocus(index) {
        var oldFocused = vm.model.appFocus;
        vm.model.appFocus = index >= 0 ? vm.model.onStage[index] : vm.model.appFocus;


        $scope.$emit('beatbox:stage:focusChange', {
          old: oldFocused,
          new: vm.model.appFocus,
          newModel: vm.model.beatApp[index]
        });


      }


      $scope.$on('beatbox:app:close',_resizeApps);
      $scope.$on('beatbox:app:open',_resizeApps);

      $scope.$on('beatbox:app:focusrequest', function (event, index) {
        setFocus(index);
      });

      $scope.$on('beatbox:stage:focusChange', function (event, data) {
        //debugger
        if (data.new === data.old) {
          //no focus change
          $log.debug('no focus changed');
        } else {
          data.new.app.controller.api.onFocus();
          if (data.old) {
            data.old.app.controller.api.onFocusLost();
          }
        }
        _resizeApps()
      });

      $scope.$on('beatbox:stage:sizeChange', function (event, data) {
        vm.model.stageWidth = data;
        $log.debug('size changed to ' + data);
        _resizeApps()
      });


      function _resizeApps() {
        var availableStageWith = vm.model.stageWidth;

        for (var i = 0; i < vm.model.onStage.length; i++) {
          if (vm.model.onStage[i].viewState != 'opened') {
            _setWitdh(vm.model.onStage[i],vm.config.minimizedSize);
            availableStageWith -= vm.config.minimizedSize;
          }else{
            var minSize = vm.model.beatApp[i].minSize || vm.config.defaultMinSize;
            if(vm.model.appFocus.index != i){
              _setWitdh(vm.model.onStage[i],minSize);
              availableStageWith-= minSize;

            }
          }

        }
        if (vm.model.appFocus) {
          _setWitdh(vm.model.appFocus,availableStageWith);
        }
      }

      function _setWitdh(app,width){
        $log.debug("Element:"+vm.model.beatApp[app.index].name+" set width "+ app.app.element.width() +" -> "+width);
        app.app.element.width(width);
        app.app.element[0].style.maxWidth= width+'px';

      }

      (function () {

      })();
    }
  }

})();
