(function () {
  'use strict';

  angular
    .module('beat')
    .directive('beatbox', beatbox);

  /** @ngInject */
  function beatbox() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/beatbox/beatbox-tpl.html',
      scope: {},
      controller: BeatboxController,
      controllerAs: 'vm',
      bindToController: true,
    };

    return directive;

    /** @ngInject */
    function BeatboxController($scope, $log, beatboxService) {
      var vm = this;
      vm.model = {
        apps: []
      };

      $scope.model = {};


      //beatboxService.pageStructure();

 /*     beatboxService.list().then(function (data) {
        angular.forEach(data, function(item){
          vm.model.apps.push(item);
        });

        //$scope.$apply();
      });
*/


      $scope.$on('beatbox:stage:focusChange', function (event, data) {
        vm.model.onStage = data;
        //$log.debug("Menu switch app:" + angular.toJson(data.newModel, true));
        if (data.newModel) {
          _broadcastEvent('beatbox:appOnStage', data.newModel);
        }

      });

      $scope.$on('beat:action:command', function(event,command){
        //$log.debug("Command invoked: " + angular.toJson(command,true));
        _broadcastEvent('beatbox:action:command',command);
      });


      function _broadcastEvent(event, data) {
        //debugger
        $scope.$broadcast(event, data);
      }

      (function(){

      })()

    }
  }

})();
