(function () {
    'use strict';

    angular
      .module('beat')
      .controller("RadarCtrl", RadarController)
      .controller("App1Ctrl", App1Controller)
      .controller("App3Ctrl", App3Controller)
      .controller("MapController", MapController);


    function RadarController($scope, $interval) {
      $scope.labelsRad = ["0-4h", "4-6h", "6-12h", "12-16h", "16-20h", "20-22h", "22-23h"];

      $scope.dataRad = [
        [65, 59, 90, 81, 56, 55, 40],
        [28, 48, 40, 19, 96, 27, 100]
      ];


      var maximum = 100;
      $scope.data = [[]];
      $scope.labels = [];
      $scope.powerOpt = {
        animation: {
          duration: 0
        },
        elements: {
          line: {
            borderWidth: 0.5
          },
          point: {
            radius: 0
          }
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: true
          }],
          gridLines: {
            display: false
          }
        },
        tooltips: {
          enabled: false
        }
      };

      // Update the dataset at 25FPS for a smoothly-animating chart
      $interval(function () {
        getLiveChartData();
      }, 80);

      function getLiveChartData() {
        if ($scope.data[0].length) {
          $scope.labels = $scope.labels.slice(1);
          $scope.data[0] = $scope.data[0].slice(1);
        }

        while ($scope.data[0].length < maximum) {
          $scope.labels.push('');
          $scope.data[0].push(getRandomValue($scope.data[0]));
        }
      }

      function getRandomValue(data) {
        var l = data.length, previous = l ? data[l - 1] : 50;
        var y = previous + Math.random() * 10 - 5;
        return y < 0 ? 0 : y > 100 ? 100 : y;
      }




    }

    function App1Controller($scope) {
      var vm = this;
      $scope.vm.date = "Clicca qui per attivare il calendario";
      $scope.vm.map = "Clicca qui per visualizzare la mappa";
      $scope.vm.note = "Clicca qui per scrivere una nota";


      $scope.rowCollection = [
        {
          col0: "fa-database",
          col1: "Lettura Contatore",
          col2: new Date(2015, 2, 2, 13, 10),
          col3: 'Ancona',
          note: ""
        },
        {
          col0: "fa-chain-broken",
          col1: "Rottura Contatore",
          col2: new Date(2014, 2, 2, 13, 10),
          col3: 'Roma',
          note: ""
        },
        {
          col0: "fa-repeat",
          col1: "Sostituzione Contatore",
          col2: new Date(2016, 2, 2, 13, 10),
          col3: 'Napoli',
          note: ""
        },
        {
          col0: "fa-repeat",
          col1: "Sostituzione Contatore",
          col2: new Date(2016, 2, 2, 13, 10),
          col3: 'Cagliari',
          note: ""
        },
        {
          col0: "fa-database",
          col1: "Lettura Contatore",
          col2: new Date(2016, 5, 12, 14, 10),
          col3: 'Perugia',
          note: ""
        },
        {
          col0: "fa-database",
          col1: "Lettura Contatore",
          col2: new Date(2016, 10, 12, 3, 10),
          col3: 'Milano',
          note: ""
        },
        {
          col0: "fa-repeat",
          col1: "Sostituzione Contatore",
          col2: new Date(2016, 2, 2, 13, 10),
          col3: 'Cascia',
          note: ""
        }


      ];


      $scope.api = {
        callbackDate: function (value) {
          $scope.vm.date = value;
        },
        callbackMap: function (value) {
          $scope.vm.map = value;
        },
        callbackNote: function (value) {
          $scope.vm.note = value;
        }

      }
    }


    function App3Controller($scope) {
      var vm = this;
      $scope.vm.date = "Clicca qui per attivare il calendario";
      $scope.vm.map = "Clicca qui per visualizzare la mappa";
      $scope.vm.note = "Clicca qui per scrivere una nota";


      $scope.rowCollection = [
        {
          col1: "Mario Rossi",
          col2: 100.12,
          col3: true,
        }, {
          col1: "Antonio Bianchi",
          col2: 670.24,
          col3: false,
        }, {
          col1: "Anna Verdi",
          col2: 80.37,
          col3: false,
        }, {
          col1: "Marco Marchi",
          col2: 1012.56,
          col3: true,
        }, {
          col1: "Fabio Rovazzi",
          col2: 2043.12,
          col3: false,
        }

      ];


      $scope.api = {
        callbackDate: function (value) {
          $scope.vm.date = value;
        },
        callbackMap: function (value) {
          $scope.vm.map = value;
        },
        callbackNote: function (value) {
          $scope.vm.note = value;
        }

      }
    }

    function MapController() {
      var vm = this;


      vm.callbackFunc = function (param) {
        console.log('I know where ' + param + ' are. ' + vm.message);
        console.log('You are at' + vm.map.getCenter());
      };
    }

    function NoteController() {
      var vm = this;

    }

  })();
