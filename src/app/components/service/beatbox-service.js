(function () {
  'use strict';

  angular
    .module('beat')
    .service('beatboxService', beatboxService);

  /** @ngInject */
  function beatboxService($q, $location, $log, $http) {


    var pageStructure = undefined;

    //Match `http://localhost:8080/pocenel/en/dashboard.page`;
    const entandoPageRegex = /.*\/([a-z]{2})\/(.*)\.page/;

    function readEntandoPageStructure(lang, page) {
      var deferred = $q.defer();

      var pageStructureURI = URI.expand("api/rs/{lang}/enel/page.json?code={page}", {lang: lang, page: page});

      $http({
        method: 'GET',
        url: pageStructureURI.toString(),
        cache: false
      }).then(function (res) {
        //$log.debug(angular.toJson(res.data.response.result.item,true));

        var items = res.data.response.result.item;
        pageStructure = {
          env: {}
        };

        pageStructure.env.lang = lang;
        pageStructure.env.page = page;

        pageStructure.env.group = items.group;
        pageStructure.env.parent = items.parentCode;
        pageStructure.env.pageModel = items.model;
        pageStructure.items = [];

        pageStructure.widgets = []
        angular.forEach(items.frames, function (item) {
          pageStructure.items.push({
            frame: item.frame,
            description: item.description,
            widget: item.widget ? item.widget.code : false
          })
          if (item.widget && pageStructure.widgets.indexOf(item.widget.code)) {
            pageStructure.widgets.push(item.widget.code)
          }
        });

        $log.debug(angular.toJson(pageStructure, true));

        deferred.resolve(pageStructure);


      }, function (err) {
        $log.error(err)
        deferred.reject(err);
      });

      return deferred.promise;
    }


    function getTemplate4WidgetURL(widgetPosition) {
      if (!pageStructure) {
        throw new Error("OMG... this can't be real!!!");
      }

      $log.debug("Reading template for widget: " + widgetPosition);


      return URI.expand("widget?frame={frame}&pageCode={page}", {frame:widgetPosition + 3, page: pageStructure.env.page});

    }

    (function () {

      var match = entandoPageRegex.exec($location.url());

      var lang = 'en';
      var page = 'dashboard';

      if (match !== null) {
        lang = match[1];
        page = match[2];
      }

      readEntandoPageStructure(lang, page);


    })();

    return {
      //pageStructure: readEntandoPageStructure,
      template4Widget: getTemplate4WidgetURL
    }

  }

})();
