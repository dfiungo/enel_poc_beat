(function () {
  'use strict';

  angular
    .module('beat',
      ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.bootstrap', 'toastr', 'chart.js', 'ngAside', 'ngMap', 'angularTrix','smart-table']);

})();
